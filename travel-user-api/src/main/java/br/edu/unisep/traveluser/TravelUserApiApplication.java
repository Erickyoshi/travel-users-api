package br.edu.unisep.traveluser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TravelUserApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TravelUserApiApplication.class, args);
	}

}
